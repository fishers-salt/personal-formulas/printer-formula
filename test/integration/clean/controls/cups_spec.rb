# frozen_string_literal: true

control 'printer-cups-clean-printer-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/cups/lpoptions') do
    it { should_not exist }
  end
end

control 'printer-cups-clean-printer-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/cups') do
    it { should_not exist }
  end
end

control 'printer-cups-clean-config-absent' do
  title 'should not exist'

  describe file('/etc/cups/cups-files.conf') do
    it { should_not exist }
  end
end

control 'printer-cups-absent-service-disabled' do
  title 'should be stopped and disabled'

  describe service('cups.socket') do
    it { should_not be_enabled }
    it { should_not be_running }
  end
end

control 'printer-cups-clean-pkg-removed' do
  title 'should not be installed'

  describe package('cups') do
    it { should_not be_installed }
  end
end

control 'printer-cups-clean-cups-user-absent' do
  title 'should not exist'

  describe user('cups') do
    it { should_not exist }
  end
end

control 'printer-cups-clean-cups-group-absent' do
  title 'should not exist'

  describe group('cups') do
    it { should_not exist }
  end
end
