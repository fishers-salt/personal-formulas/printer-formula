# frozen_string_literal: true

control 'printer-cups-install-cups-group-managed' do
  title 'should be present'

  describe group('cups') do
    it { should exist }
    its('gid') { should eq 209 }
  end
end

control 'printer-cups-install-cups-user-managed' do
  title 'should be present'

  describe user('cups') do
    it { should exist }
    its('uid') { should eq 209 }
    its('gid') { should eq 209 }
    its('group') { should eq 'cups' }
    its('home') { should eq '/' }
    its('shell') { should eq '/usr/bin/nologin' }
  end
end

control 'printer-cups-install-pkgs-installed' do
  title 'should be installed'

  describe package('cups') do
    it { should be_installed }
  end
end

control 'printer-cups-install-service-enabled' do
  title 'should be running and enabled'

  describe service('cups.socket') do
    it { should be_enabled }
    it { should be_running }
  end
end

control 'printer-cups-install-config-managed' do
  title 'should match desired lines'

  describe file('/etc/cups/cups-files.conf') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'cups' }
    its('mode') { should cmp '0640' }
    its('content') { should include('#  Your changes will be overwritten.') }
    its('content') { should include('User 209') }
    its('content') { should include('Group 209') }
  end
end

control 'printer-cups-install-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'printer-cups-install-printer-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/cups') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'printer-cups-install-printer-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/cups/lpoptions') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('#  Your changes will be overwritten.') }
    its('content') do
      should include('Default HP_OfficeJet_Pro_8710 sides=two-sided-long-edge')
    end
  end
end
