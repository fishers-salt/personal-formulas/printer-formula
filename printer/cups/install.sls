# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as printer with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set cups = printer['cups'] %}

printer-cups-install-cups-group-managed:
  group.present:
    - name: {{ cups.group.name }}
    - gid: {{ cups.group.gid }}

printer-cups-install-cups-user-managed:
  user.present:
    - name: {{ cups.user.name }}
    - uid: {{ cups.user.uid }}
    - gid: {{ cups.group.gid }}
    - fullname: cups helper user
    - shell: /usr/bin/nologin
    - home: /

printer-cups-install-pkg-installed:
  pkg.installed:
    - name: {{ cups.pkg.name }}

printer-cups-install-service-enabled:
  service.running:
    - name: {{ cups.service.name }}
    - enable: True

printer-cups-install-config-managed:
  file.managed:
    - name: /etc/cups/cups-files.conf
    - source: {{ files_switch(['cups-files.conf.tmpl'],
                lookup='printer-cups-install-config-managed',
                )
              }}
    - mode: '0640'
    - user: root
    - group: {{ cups.group.name }}
    - template: jinja
    - context:
        gid: {{ cups.group.gid }}
        uid: {{ cups.user.uid }}
    - require:
      - printer-cups-install-pkg-installed
      - printer-cups-install-cups-group-managed
      - printer-cups-install-cups-user-managed

{% if salt['pillar.get']('printer-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_printer', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

printer-cups-install-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

printer-cups-install-printer-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/cups
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - printer-cups-install-user-{{ name }}-present

{%- set default_printer = printer.default_printer %}
printer-cups-install-printer-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/cups/lpoptions
    - source: {{ files_switch([
                  name ~ '-lpoptions.tmpl',
                  'lpoptions.tmpl'],
                lookup='printer-cups-install-printer-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        default_printer: {{ default_printer }}
    - require:
      - printer-cups-install-printer-dir-{{ name }}-managed

{% endif %}
{% endfor %}
{% endif %}
