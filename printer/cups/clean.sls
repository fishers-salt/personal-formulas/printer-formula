# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as printer with context %}

{% if salt['pillar.get']('printer-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_printer', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

printer-cups-clean-printer-config-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/cups/lpoptions

printer-cups-clean-printer-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/cups

{% endif %}
{% endfor %}
{% endif %}

printer-cups-absent-config-absent:
  file.absent:
    - name: /etc/cups/cups-files.conf

{%- set cups = printer['cups'] %}

printer-cups-absent-service-disabled:
  service.dead:
    - name: {{ cups.service.name }}
    - enable: False

printer-cups-clean-pkg-removed:
  pkg.removed:
    - name: {{ cups.pkg.name }}

printer-cups-clean-cups-user-absent:
  user.absent:
    - name: {{ cups.user.name }}

printer-cups-clean-cups-group-absent:
  group.absent:
    - name: {{ cups.group.name }}
