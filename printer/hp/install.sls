# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as printer with context %}

{%- set hp = printer.get('hp', none) %}

{%- if hp is not none %}
printer-hp-install-pkgs-installed:
  pkg.installed:
    - pkgs: {{ hp.pkgs }}

{# Can't install hplip in test env - salt creates these files manually #}
{#
printer-hp-install-clean-salt-artifacts:
  cmd.run:
    - name: rm -rf /usr/lib/python3.10/site-packages/distro && rm -rf /usr/lib/python3.10/site-packages/distro-1.8.0.dist-info && rm /usr/bin/distro
    - onfail:
      - pkg: printer-hp-install-pkgs-installed
    - onfail_stop: false
  pkg.installed:
    - pkgs: {{ hp.pkgs }}
#}

printer-hp-install-setup-hp-printer:
  cmd.run:
    - name: echo 1 | hp-setup --interactive --auto --type=print --printer={{ hp.name }} -x {{ hp.ip_address }}
    - creates: /etc/cups/ppd/HP_OfficeJet_Pro_8710.ppd
    - require:
      - printer-hp-install-pkgs-installed

{%- endif %}
